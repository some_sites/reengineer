---
title: "adding mermaid to hugo with nightfall theme"
date: 2023-07-23
draft: false
tags:
  - mermaid
  - hugo
  - theme
  - diagram
  - blog
---

So, to get this small little blog of mine up and running, I chose:
 - [Hugo](https://gohugo.io) (what is not to like)
 - [Nightfall theme](https://themes.gohugo.io/themes/hugo-theme-nightfall/) (minimal and dark)
 - [Mermaid.js](https://mermaid.js.org/) (diagrams in markdown .....)

<!--more-->

## hugo and nightfall 1st.

There is a lot of information online to get a person started quickly with hugo and themes.

##  mermaid.js 2nd

I love Mermaid because I can draw diagrams like this.

```mermaid
graph TD

sum[ 1 + x] -->  solution{= 2 ?} -->|correct| correct[x == 1]
solution -->|wrong| wrong[x !=1 ]
```

With markdown like this.

````markdown
```mermaid
graph TD

sum[ 1 + x] -->  solution{= 2 ?} -->|correct| correct[x == 1]
solution -->|wrong| wrong[x !=1 ]
```
````

### The Steps

Go to the nightfall theme dir from your base hugo dir

```bash
cd themes/nightfall/
```

#### 1. create the render codeblock

```bash
mkdir -p layouts/_default/_markup/
cat <<EOF >> layouts/_default/_markup/render-codeblock-mermaid.html
<pre class="mermaid">
  {{- .Inner | safeHTML }}
</pre>
{{ .Page.Store.Set "hasMermaid" true }}
EOF
```

#### 2. update  *layouts/_default/single.html*

```html
{{ define "title"}}
{{ .Title }} | {{ .Site.Params.author }}
{{ end }}

{{ define "main" }}
<div class="postWrapper">
    <h1>{{ .Title }}</h1>
    {{ if .Params.showMetadata | default true }}
    <section class="postMetadata">
        <dl>
            {{ with .GetTerms "tags" }}
                {{ partial "taxonomy/tags.html" . }}
            {{ end }}
            {{ with .GetTerms "authors" }}
                {{ partial "taxonomy/authors.html" . }}
            {{ end }}
            {{ with .GetTerms "categories" }}
                {{ partial "taxonomy/categories.html" . }}
            {{ end }}
            {{ if .Site.Params.published | default true }}
                <dt>{{ i18n "published" }}</dt>
                {{ $formattedDate := .Date.Format "2006-01-02" }}
                <dd><time datetime="{{ $formattedDate }}">{{ .Date | time.Format ":date_long" }}</time></dd>
            {{ end }}
            {{ if .Site.Params.readingTime | default true }}
                <dt>{{ i18n "reading_time" }}</dt>
                <dd>{{ i18n "reading_time_desc" .ReadingTime }}</dd>
            {{ end }}
        </dl>
    </section>
    {{ end }}
    <div>
        {{ .Content }}
        {{ if .Page.Store.Get "hasMermaid" }}
            <script type="module">
                import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.esm.min.mjs';
                mermaid.initialize({ startOnLoad: true });
            </script>
{{ end }}
```

