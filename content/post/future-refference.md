---
title: "Future Refference"
date: 2024-01-11T17:25:40+02:00
draft: false
---

I am just putting this piece of code here for future reference because I keep needing to rewrite or Google for it.

It is a "simple" way to get terminal key presses in Linux-based systems.

<!--more-->

```go
package main

import (
    "fmt"
    "os"
    "os/exec"
)

func main() {
    exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
    exec.Command("stty", "-F", "/dev/tty", "-echo").Run()

    var b []byte = make([]byte, 1)
    for {
        os.Stdin.Read(b)
        fmt.Println("I got the byte", b, "("+string(b)+")")
    }
}
```
