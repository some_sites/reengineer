+++
title = "About"
description = "just a small space"
date = "2023-07-23"
aliases = ["about"]
+++

Welcome to my little corner of the interwebs!
Here, I'll be sharing my thoughts on software engineering and the systems that surround them.
I believe in open source and the power of collaboration and the freedom to explore and modify software.

P.S. I am an Aspie trying to survive in the corporate world :D
