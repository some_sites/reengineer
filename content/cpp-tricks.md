---
title: "C++ Tricks"
date: 2025-01-01T17:21:12+02:00
draft: false
---

A few months ago, I challenged myself to port one of my large personal projects from Go to C++. The motivation? To explore improvements in size, speed, and—if I’m being honest—because I just felt like it.

C++ is a fascinating language to work in. It’s compiled, statically typed, time-tested, and incredibly robust. It’s used practically everywhere, from operating systems to game engines. However, it does come with some trade-offs. For instance, I’m still adjusting to the slower compile times (and I won’t even get started on the thought of trying Rust!).

While I’ve thoroughly enjoyed diving into C++, I find myself missing a few features and conveniences from Go. As a result, I’ve decided to remake and catalog some of those functionalities and aspects of Go that I appreciated.

While I’ve enjoyed diving into C++, I find myself missing a few features and conveniences from Go. As a result, I’ve decided to experiment with ways to make life in C++ more enjoyable by bringing in a bit of the Go-style usefulness I appreciate so much, along with other creative experiments.


 - [defer](/post/cpp-tricks-defer/)
